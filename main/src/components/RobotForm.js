import React, { Component } from 'react'
class RobotForm extends Component {

    constructor(props) {
        super(props)
        this.state = {
            name: '',
            type: '',
            mass: ''
        }
        this.add = () => {
            this.props.onAdd({
                type: this.state.type,
                name: this.state.name,
                mass: this.state.mass
            })
        }

        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
    }
    render(){
        return(
        <div>
            <input id="type" type="text" name="type"onChange={this.handleChange}/>
            <input id="name" type="text" name="name" onChange={this.handleChange}/>
            <input id="mass" type="text" name="mass" onChange={this.handleChange}/>
            <input id="" type="button" value="add" onClick={this.add}/>

        </div>

        )}

} 

export default RobotForm;